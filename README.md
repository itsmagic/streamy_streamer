# Streamy Streamer -- _for all your multicast testing needs_

## Intent
A tool to test SVSi multicast audio streams


## Features
* Receive multicast streams from encoders
* Send your microphone audio to decoders
