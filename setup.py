import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="streamy_streamer",
    version="1.0.1",
    author="Jim Maciejewski",
    author_email="magicsoftware@ornear.com",
    description="Multicast streaming testing tool",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/itsmagic/streamy_streamer",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=['ipaddress', 'PyAudio', 'PyDispatcher', 'wxpython', 'pywin32', 'requests', 'wmi'],
    python_requires='>=3.6',
)