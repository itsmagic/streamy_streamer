# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 4.0.0-0-g0efcecf)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class StreamyFrame
###########################################################################

class StreamyFrame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.Size( 490,480 ), wx.DefaultSize )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel1 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer2 = wx.BoxSizer( wx.VERTICAL )

		bSizer15 = wx.BoxSizer( wx.HORIZONTAL )

		sbSizer5 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel1, wx.ID_ANY, u"Transmitter" ), wx.VERTICAL )

		sbSizer6 = wx.StaticBoxSizer( wx.StaticBox( sbSizer5.GetStaticBox(), wx.ID_ANY, u"Stream ID" ), wx.VERTICAL )

		bSizer17 = wx.BoxSizer( wx.VERTICAL )

		self.tx_txt_input = wx.TextCtrl( sbSizer6.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.tx_txt_input.SetToolTip( u"Please enter the Stream ID or Multicast Group.\nA red backgroud indicates invalid values.\n" )

		bSizer17.Add( self.tx_txt_input, 0, wx.ALL|wx.EXPAND, 5 )


		sbSizer6.Add( bSizer17, 1, wx.EXPAND, 5 )

		bSizer18 = wx.BoxSizer( wx.VERTICAL )

		self.tx_interleave_chk = wx.CheckBox( sbSizer6.GetStaticBox(), wx.ID_ANY, u"Interleave", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer18.Add( self.tx_interleave_chk, 0, wx.ALL, 5 )


		sbSizer6.Add( bSizer18, 1, wx.EXPAND, 5 )

		bSizer182 = wx.BoxSizer( wx.VERTICAL )

		tx_ip_choice_cmbChoices = []
		self.tx_ip_choice_cmb = wx.Choice( sbSizer6.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, tx_ip_choice_cmbChoices, 0 )
		self.tx_ip_choice_cmb.SetSelection( 0 )
		bSizer182.Add( self.tx_ip_choice_cmb, 0, wx.ALL|wx.EXPAND, 5 )


		sbSizer6.Add( bSizer182, 1, wx.EXPAND, 5 )


		sbSizer5.Add( sbSizer6, 0, wx.ALL|wx.EXPAND, 5 )

		sbSizer9 = wx.StaticBoxSizer( wx.StaticBox( sbSizer5.GetStaticBox(), wx.ID_ANY, u"Multicast" ), wx.VERTICAL )

		bSizer111 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText12 = wx.StaticText( sbSizer9.GetStaticBox(), wx.ID_ANY, u"Address", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText12.Wrap( -1 )

		self.m_staticText12.SetMinSize( wx.Size( 50,-1 ) )

		bSizer111.Add( self.m_staticText12, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.tx_group_txt = wx.TextCtrl( sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer111.Add( self.tx_group_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer9.Add( bSizer111, 1, wx.EXPAND, 5 )

		bSizer12 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText131 = wx.StaticText( sbSizer9.GetStaticBox(), wx.ID_ANY, u"ID", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText131.Wrap( -1 )

		self.m_staticText131.SetMinSize( wx.Size( 50,-1 ) )

		bSizer12.Add( self.m_staticText131, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.tx_id_txt = wx.TextCtrl( sbSizer9.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer12.Add( self.tx_id_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer9.Add( bSizer12, 1, wx.EXPAND, 5 )


		sbSizer5.Add( sbSizer9, 0, wx.EXPAND|wx.ALL, 5 )

		sbSizer8 = wx.StaticBoxSizer( wx.StaticBox( sbSizer5.GetStaticBox(), wx.ID_ANY, u"TX Controls" ), wx.VERTICAL )

		sbSizer8.SetMinSize( wx.Size( -1,80 ) )
		mic_cmbChoices = []
		self.mic_cmb = wx.Choice( sbSizer8.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, mic_cmbChoices, 0 )
		self.mic_cmb.SetSelection( 0 )
		sbSizer8.Add( self.mic_cmb, 0, wx.ALL|wx.EXPAND, 5 )

		self.mic_btn = wx.Button( sbSizer8.GetStaticBox(), wx.ID_ANY, u"Send Mic", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer8.Add( self.mic_btn, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )


		sbSizer5.Add( sbSizer8, 0, wx.ALL|wx.EXPAND, 5 )

		sbSizer10 = wx.StaticBoxSizer( wx.StaticBox( sbSizer5.GetStaticBox(), wx.ID_ANY, u"Packets" ), wx.VERTICAL )

		bSizer16 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText15 = wx.StaticText( sbSizer10.GetStaticBox(), wx.ID_ANY, u"Sent", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText15.Wrap( -1 )

		self.m_staticText15.SetMinSize( wx.Size( 50,-1 ) )

		bSizer16.Add( self.m_staticText15, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.tx_sent_txt = wx.TextCtrl( sbSizer10.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer16.Add( self.tx_sent_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer10.Add( bSizer16, 0, wx.EXPAND, 5 )


		sbSizer5.Add( sbSizer10, 0, wx.EXPAND|wx.ALL, 5 )


		bSizer15.Add( sbSizer5, 1, wx.ALL, 5 )

		sbSizer4 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel1, wx.ID_ANY, u"Receiver" ), wx.VERTICAL )

		sbSizer1 = wx.StaticBoxSizer( wx.StaticBox( sbSizer4.GetStaticBox(), wx.ID_ANY, u"Stream ID" ), wx.VERTICAL )

		bSizer19 = wx.BoxSizer( wx.VERTICAL )

		self.rx_txt_input = wx.TextCtrl( sbSizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.rx_txt_input.SetToolTip( u"Please enter the Stream ID or Multicast Group.\nA red backgroud indicates invalid values." )

		bSizer19.Add( self.rx_txt_input, 0, wx.ALL|wx.EXPAND, 5 )


		sbSizer1.Add( bSizer19, 1, wx.EXPAND, 5 )

		bSizer20 = wx.BoxSizer( wx.VERTICAL )

		self.rx_interleave_chk = wx.CheckBox( sbSizer1.GetStaticBox(), wx.ID_ANY, u"Interleave", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer20.Add( self.rx_interleave_chk, 0, wx.ALL, 5 )


		sbSizer1.Add( bSizer20, 1, wx.EXPAND, 5 )

		bSizer192 = wx.BoxSizer( wx.VERTICAL )

		rx_ip_choice_cmbChoices = []
		self.rx_ip_choice_cmb = wx.Choice( sbSizer1.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, rx_ip_choice_cmbChoices, 0 )
		self.rx_ip_choice_cmb.SetSelection( 0 )
		bSizer192.Add( self.rx_ip_choice_cmb, 0, wx.ALL|wx.EXPAND, 5 )


		sbSizer1.Add( bSizer192, 1, wx.EXPAND, 5 )


		sbSizer4.Add( sbSizer1, 0, wx.ALL|wx.EXPAND, 5 )

		sbSizer3 = wx.StaticBoxSizer( wx.StaticBox( sbSizer4.GetStaticBox(), wx.ID_ANY, u"Multicast" ), wx.VERTICAL )

		bSizer7 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText4 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Address", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText4.Wrap( -1 )

		self.m_staticText4.SetMinSize( wx.Size( 50,-1 ) )

		bSizer7.Add( self.m_staticText4, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.rx_group_txt = wx.TextCtrl( sbSizer3.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer7.Add( self.rx_group_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer3.Add( bSizer7, 1, wx.EXPAND, 5 )

		bSizer91 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText6 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"ID", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText6.Wrap( -1 )

		self.m_staticText6.SetMinSize( wx.Size( 50,-1 ) )

		bSizer91.Add( self.m_staticText6, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.rx_id_txt = wx.TextCtrl( sbSizer3.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer91.Add( self.rx_id_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer3.Add( bSizer91, 1, wx.EXPAND, 5 )


		sbSizer4.Add( sbSizer3, 0, wx.ALL|wx.EXPAND, 5 )

		sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( sbSizer4.GetStaticBox(), wx.ID_ANY, u"RX Controls" ), wx.HORIZONTAL )

		sbSizer2.SetMinSize( wx.Size( -1,80 ) )
		bSizer11 = wx.BoxSizer( wx.VERTICAL )

		bSizer181 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText9 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Sample Rate", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText9.Wrap( -1 )

		bSizer181.Add( self.m_staticText9, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		sample_rate_textChoices = [ u"32000", u"44100", u"48000" ]
		self.sample_rate_text = wx.Choice( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, sample_rate_textChoices, 0 )
		self.sample_rate_text.SetSelection( 2 )
		self.sample_rate_text.Enable( False )

		bSizer181.Add( self.sample_rate_text, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.play_btn = wx.Button( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Play", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer181.Add( self.play_btn, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer11.Add( bSizer181, 0, 0, 5 )

		bSizer191 = wx.BoxSizer( wx.HORIZONTAL )

		self.record_btn = wx.CheckBox( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Record to file", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer191.Add( self.record_btn, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.record_size_txt = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.record_size_txt.Wrap( -1 )

		bSizer191.Add( self.record_size_txt, 0, wx.ALL, 5 )


		bSizer11.Add( bSizer191, 1, wx.EXPAND, 5 )


		sbSizer2.Add( bSizer11, 1, wx.EXPAND, 5 )


		sbSizer4.Add( sbSizer2, 0, wx.ALL|wx.EXPAND, 5 )

		sbSizer91 = wx.StaticBoxSizer( wx.StaticBox( sbSizer4.GetStaticBox(), wx.ID_ANY, u"Packets" ), wx.VERTICAL )

		bSizer9 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText11 = wx.StaticText( sbSizer91.GetStaticBox(), wx.ID_ANY, u"Received", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText11.Wrap( -1 )

		self.m_staticText11.SetMinSize( wx.Size( 50,-1 ) )

		bSizer9.Add( self.m_staticText11, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.rx_rcvd_txt = wx.TextCtrl( sbSizer91.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer9.Add( self.rx_rcvd_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer91.Add( bSizer9, 1, wx.EXPAND, 5 )

		bSizer10 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText13 = wx.StaticText( sbSizer91.GetStaticBox(), wx.ID_ANY, u"Dropped", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText13.Wrap( -1 )

		self.m_staticText13.SetMinSize( wx.Size( 50,-1 ) )

		bSizer10.Add( self.m_staticText13, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.rx_dropped_txt = wx.TextCtrl( sbSizer91.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer10.Add( self.rx_dropped_txt, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer91.Add( bSizer10, 1, wx.EXPAND, 5 )


		sbSizer4.Add( sbSizer91, 0, wx.ALL|wx.EXPAND, 5 )


		bSizer15.Add( sbSizer4, 1, wx.ALL, 5 )


		bSizer2.Add( bSizer15, 1, wx.EXPAND, 5 )


		self.m_panel1.SetSizer( bSizer2 )
		self.m_panel1.Layout()
		bSizer2.Fit( self.m_panel1 )
		bSizer1.Add( self.m_panel1, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()
		bSizer1.Fit( self )
		self.m_menubar1 = wx.MenuBar( 0 )
		self.m_menu1 = wx.Menu()
		self.m_menuItem2 = wx.MenuItem( self.m_menu1, wx.ID_ANY, u"Exit", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu1.Append( self.m_menuItem2 )

		self.m_menubar1.Append( self.m_menu1, u"File" )

		self.m_menu2 = wx.Menu()
		self.m_menuItem3 = wx.MenuItem( self.m_menu2, wx.ID_ANY, u"API Documentation", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu2.Append( self.m_menuItem3 )

		self.m_menubar1.Append( self.m_menu2, u"Help" )

		self.SetMenuBar( self.m_menubar1 )


		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.on_close )
		self.tx_txt_input.Bind( wx.EVT_TEXT, self.on_tx_text_entry )
		self.tx_interleave_chk.Bind( wx.EVT_CHECKBOX, self.on_tx_text_entry )
		self.tx_ip_choice_cmb.Bind( wx.EVT_CHOICE, self.on_tx_ip )
		self.mic_btn.Bind( wx.EVT_BUTTON, self.on_mic_toggle )
		self.rx_txt_input.Bind( wx.EVT_TEXT, self.on_rx_text_entry )
		self.rx_interleave_chk.Bind( wx.EVT_CHECKBOX, self.on_rx_text_entry )
		self.rx_ip_choice_cmb.Bind( wx.EVT_CHOICE, self.on_rx_ip )
		self.play_btn.Bind( wx.EVT_BUTTON, self.on_play_toggle )
		self.record_btn.Bind( wx.EVT_CHECKBOX, self.on_record_toggle )
		self.Bind( wx.EVT_MENU, self.on_close, id = self.m_menuItem2.GetId() )
		self.Bind( wx.EVT_MENU, self.on_documentation, id = self.m_menuItem3.GetId() )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def on_close( self, event ):
		event.Skip()

	def on_tx_text_entry( self, event ):
		event.Skip()


	def on_tx_ip( self, event ):
		event.Skip()

	def on_mic_toggle( self, event ):
		event.Skip()

	def on_rx_text_entry( self, event ):
		event.Skip()


	def on_rx_ip( self, event ):
		event.Skip()

	def on_play_toggle( self, event ):
		event.Skip()

	def on_record_toggle( self, event ):
		event.Skip()


	def on_documentation( self, event ):
		event.Skip()


###########################################################################
## Class AboutFrame
###########################################################################

class AboutFrame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 800,700 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )


		self.Centre( wx.BOTH )

	def __del__( self ):
		pass


