import pyaudio
from threading import Thread
from pydispatch import dispatcher
import time



class AudioPlayer(Thread):
    """The Audio Player thread"""

    def __init__(self, sample_rate: int):
        self.shutdown = False
        dispatcher.connect(self.shutdown_requested, signal="PlayShutdown")
        self.p = pyaudio.PyAudio()
        self.stream = self.p.open(format=8,
                                  channels=2,
                                  rate=sample_rate,
                                  output=True)
        Thread.__init__(self, name="Audio Player")

    def shutdown_requested(self):
        self.shutdown = True

    def run(self):
        dispatcher.connect(self.play, signal="Audio", sender=dispatcher.Any)
        while not self.shutdown:
            # need this sleep to reduce blocking
            time.sleep(.1)
            pass
        self.stream.stop_stream()
        self.stream.close()
        self.p.terminate()
        dispatcher.disconnect(self.play, signal="Audio")

    def play(self, sender, data):
        """Play the received frame"""
        # print(f"{time.time()} : {data['sequence']}")
        if not self.shutdown:
            self.stream.write(data)



def main():
    import stream_listener
    listener = stream_listener.GetStream(multicast_group="239.255.165.18")
    listener.setDaemon(True)
    listener.start()



    test = PlayStream()
    test.setDaemon(True)
    test.start()

    time.sleep(10)

    test.shutdown = True
    test.join()

    listener.shutdown = True
    listener.join()


if __name__ == '__main__':
    main()
