import array
import binascii
import socket
# import struct
import time
from threading import Thread
from pydispatch import dispatcher


class MulticastTransmitter(Thread):
    """The Multicast Transmitter thread"""

    def __init__(self, interface, multicast_group, multicast_port=50003, stream_number=0):
        self.shutdown = False
        self.interface = interface
        self.multicast_group = multicast_group
        self.multicast_port = int(multicast_port)
        self.stream_num = stream_number
        self.packet_count = 0
        self.block_count = 0
        Thread.__init__(self, name="Multicast Transmitter")

    def run(self):
        self.server_address = (self.multicast_group, self.multicast_port)
        # Create the socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # Bind to the server address
        self.sock.settimeout(0.2)
        self.sock.bind((self.interface, 0))
        # Join
        # group = socket.inet_aton(self.multicast_group)
        # mreq = struct.pack('4sL', group, socket.INADDR_ANY)
        # ttl = struct.pack('b', 1)
        # self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)
        # print 'joining: ', repr(mreq)
        # sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
        # self.sock.setblocking(0)
        dispatcher.connect(self.send_packet, signal="Audio_In", sender=dispatcher.Any)
        while not self.shutdown:
            time.sleep(.1)
            pass

    def send_packet(self, sender, data):
        # Add header
        # print repr(data)
        # quit()

        # y = array.array('h', data)
        # y.byteswap()
        # # print repr(data[:20]), repr(y[:20])
        # data = binascii.hexlify(y).decode('hex')
        # print repr(data)
        # quit()
        
        # data = y
        # block = bytearray(data)
        blocks = [data[0:1024], data[1024:2048], data[2048:3072], data[3072:4096]]
        for block in blocks:
            header = '023c53565349'  # SVSi's header
            item10, item6 = divmod(int(self.stream_num), 256)
            # print(item6, item10, self.stream_num)
            header += hex(item6)[2:].zfill(2)
            
            
            # header += ('00' * 2)
            header += '0020'
            # header += hex(item10)[2:].zfill(2)
            # We need to check if mod is returning 0, so we can put an ff in 
            # print(f"Block count: {self.block_count}")
            if self.packet_count == 256:
                self.packet_count = 0
            header += hex(self.packet_count)[2:].zfill(2)
            # if self.block_count != 0 and (self.block_count % 255) == 0:
            #     print("adding ff")
            #     header += hex(255)[2:].zfill(2)
            # else:
            #     header += hex(self.block_count % 255)[2:].zfill(2)
            header += hex(item10)[2:].zfill(2)
            header += ('00' * 3)
            
            # print('Send Header: ', header)
            # print repr(block.encode('hex'))
            fake_block = '00' * 1024
            # print repr(fake_block)
            # quit()
            audio = bytearray(block)
            audio[0::2], audio[1::2] = audio[1::2], audio[0::2]
            frame = bytes(audio)
            packet = binascii.unhexlify(header) + frame
            # binascii.unhexlify(fake_block)
            # print 'block: ', len(packet)
            try:
                self.sock.sendto(packet, self.server_address)
            except Exception as error:
                print(f"Unable to send packet: {error}")
            self.block_count += 1


def main():
    test = SendStream('239.255.165.13')
    test.setDaemon(True)
    test.start()
    test.shutdown = True
    test.join()


if __name__ == '__main__':
    main()
