from threading import Thread
from pydispatch import dispatcher
import wave
import time

class AudioRecorder(Thread):
    """The Audio Record thread"""

    def __init__(self, file_name):
        self.shutdown = False
        self.file_name = file_name
        self.size = 0
        Thread.__init__(self, name="Audio Recorder")

    def run(self):
        dispatcher.connect(self.record, signal="Audio", sender=dispatcher.Any)
        self.wavfile = wave.open(self.file_name, 'w')
        self.wavfile.setparams((2, 2, 48000, 0, 'NONE', 'NONE'))
        while not self.shutdown:
            time.sleep(.1)
            pass
        self.wavfile.close()

    def record(self, signal, data):
        """Record the audio"""
        if not self.shutdown:
            self.size += len(data)
            self.wavfile.writeframes(data)


def main():
    pass


if __name__ == '__main__':
    main()
