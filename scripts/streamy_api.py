
import socket
import time
import json
from collections import OrderedDict
from threading import Thread


# import curses
# import curses.textpad


class StreamAPI(Thread):
    """The StreamAPI thread"""

    def __init__(self, parent, api_port=37000):
        self.parent = parent
        self.shutdown = False
        self.api_port = api_port
        Thread.__init__(self, name="Streamy API")

    def run(self):
        TCP_IP = ''
        TCP_PORT = self.api_port
        BUFFER_SIZE = 1024  # Normally 1024, but we want fast response

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((TCP_IP, TCP_PORT))
        while not self.shutdown:
            sock.listen(5)

            conn, addr = sock.accept()
            # print 'Connection address:', addr
            Thread(target=self.socket_listener, args=(conn, BUFFER_SIZE)).start()
            # sock.setblocking(0)

    def socket_listener(self, conn, BUFFER_SIZE):
        command = b""
        try:
            while True:
                data = conn.recv(BUFFER_SIZE)
                if not data:
                    continue
                # print "received data:", data
                command += data
                if b'\r\n' in command:
                    result = self.process_command(command.decode())
                    # print 'Result: ', repr(result)
                    conn.send(result.encode())
                    command = b""
            # conn.send('Ok')  # echo
        except Exception as error:
            if error.args[0] == 10035:
                # no data detected
                # lets wait a bit and check again
                time.sleep(.1)
                pass
            elif error.args[0] == 10054:
                # Looks like the client closed the connection
                conn.close()
                return
            else:
                print("Error getting data: ", error)
        conn.close()

    def process_command(self, command):
        """Processes the command"""
        # print "Command: ", command
        if command[:3] == "set":
            try:
                api_command, api_arg = command.split()
            except ValueError:
                return json.dumps({'error': "Set commands require an value or argument"})
            my_command = getattr(self, api_command, None)
            if callable(my_command):
                return my_command(api_arg)
            return json.dumps({'error': "Unknown Command"})
        else:
            api_command = command.strip()
            my_command = getattr(self, api_command, None)
            if callable(my_command):
                return my_command()
            return json.dumps({'error': "Unknown Command"})

    def get_status(self):
        """Return the status in a json string"""
        rx_stream_txt = self.parent.rx_txt_input.GetValue()
        if rx_stream_txt == "Waiting for stream":
            rx_stream = ""
            rx_status = "Waiting for stream"
        else:
            rx_stream = rx_stream_txt
            rx_status = "Receiving stream"

        result = {'rx_stream_id': self.parent.rx_txt_input.GetValue(),
                  'tx_stream_id': self.parent.tx_txt_input.GetValue(),
                  'rx_multicast_address': self.parent.rx_group_txt.GetValue(),
                  'tx_multicast_address': self.parent.tx_group_txt.GetValue(),
                  'rx_multicast_stream_number': rx_stream,
                  'tx_multicast_stream_number': self.parent.tx_id_txt.GetValue(),
                  'rx_interleave_enabled': self.parent.rx_interleave_chk.GetValue(),
                  'tx_interleave_enabled': self.parent.tx_interleave_chk.GetValue(),
                  'tx_mic_enable': self.parent.mic_recording_on,
                  'rx_record_enable': self.parent.recording,
                  'rx_play_enable': self.parent.playing,
                  'tx_packets_sent': self.parent.tx_sent_txt.GetValue(),
                  'rx_packets_received': self.parent.rx_rcvd_txt.GetValue(),
                  'rx_packets_dropped': self.parent.rx_dropped_txt.GetValue(),
                  'rx_status': rx_status}

        neat_result = OrderedDict(sorted(result.items(), key=lambda t: t[0]))
        return json.dumps(neat_result)

    def status(self):
        return self.get_status()

    def set_rx_stream(self, stream):
        """Sets RX Stream"""
        self.parent.rx_txt_input.SetValue(stream)
        return self.get_status()

    def set_rx_interleave(self, enabled):
        """Enable/Disable Interleave on RX"""
        try:
            value = bool(int(enabled))
            self.parent.rx_interleave_chk.SetValue(value)
            self.parent.on_rx_text_entry(None)
            return self.get_status()
        except Exception as error:
            return json.dumps({'error': "set_rx_interleave only takes 1 or 0",
                               'invalid': enabled,
                               'system_error': str(error)})

    def set_tx_stream(self, stream):
        """Sets RX Stream"""
        self.parent.tx_txt_input.SetValue(stream)
        return self.get_status()

    def set_tx_interleave(self, enabled):
        """Enable/Disable Interleave on TX"""
        try:
            value = bool(int(enabled))
            self.parent.tx_interleave_chk.SetValue(value)
            self.parent.on_tx_text_entry(None)
            return self.get_status()
        except Exception as error:
            return json.dumps({'error': "set_tx_interleave only takes 1 or 0",
                               'invalid': enabled,
                               'system_error': str(error)})

    def tx_mic_start(self):
        if self.parent.mic_recording_on:
            return json.dumps({'error': "Mic already started"})
        else:
            self.parent.on_mic_toggle(None)
        return self.get_status()

    def tx_mic_stop(self):
        if not self.parent.mic_recording_on:
            return json.dumps({'error': "Mic already stopped"})
        else:
            self.parent.on_mic_toggle(None)
        return self.get_status()

    def rx_play_start(self):
        if self.parent.playing:
            return json.dumps({'error': "RX play already started"})
        else:
            self.parent.on_play_toggle(None)
        return self.get_status()

    def rx_play_stop(self):
        if not self.parent.playing:
            return json.dumps({'error': "RX play already stopped"})
        else:
            self.parent.on_play_toggle(None)
        return self.get_status()

    # def rx_record_start(self):
    #     if self.parent.recording:
    #         return json.dumps({'error': "RX record already started"})
    #     else:
    #         self.parent.on_record_toggle(None)
    #     return self.get_status()

    # def rx_record_stop(self):
    #     if not self.parent.recording:
    #         return json.dumps({'error': "RX record already stopped"})
    #     else:
    #         self.parent.on_record_toggle(None)
    #     return self.get_status()


def main():

    test = StreamAPI(None)
    test.setDaemon(True)
    test.start()
    for i in range(20):
        time.sleep(1)
    test.shutdown = True
    test.join()


if __name__ == '__main__':
    main()
