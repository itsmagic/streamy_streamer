import datetime
import ipaddress
from dataclasses import dataclass, field


@dataclass
class InterfaceConfig:
    index: int = 0
    interface_index: int = 0
    ip_enabled: bool = False
    description: str = ''
    ip_addresses: list = field(default_factory=list)
    subnet_addresses: list = field(default_factory=list)
    gateway_addresses: list = field(default_factory=list)
    dhcp_enabled: bool = True
    broadcast_addresses: list = field(default_factory=list)

    def populate_broadcast_addresses(self):
        for i, ip in enumerate(self.ip_addresses):
            try:
                my_net = ipaddress.IPv4Network(ip + '/' + self.subnet_addresses[i], strict=False)
            except Exception:
                # Not IPv4
                self.broadcast_addresses.append(None)
                continue
            self.broadcast_addresses.append(str(my_net.broadcast_address))
