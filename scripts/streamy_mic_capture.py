import pyaudio
from threading import Thread
from pydispatch import dispatcher
import time
import logging

logger = logging.getLogger(name="MicCapture")
logger.setLevel(logging.DEBUG)

class MicCapture(Thread):
    """The Microphone Capture thread"""

    def __init__(self, num_channels, mic_index):
        self.shutdown = False
        self.num_channels = num_channels
        self.mic_index = mic_index
        Thread.__init__(self, name="Microphone Capture")

    def run(self):
        self.p = pyaudio.PyAudio()
        self.stream = self.p.open(format=pyaudio.paInt16,
                                  channels=self.num_channels,
                                  rate=48000,
                                  input=True,
                                  frames_per_buffer=1024,
                                  stream_callback=self.callback,
                                  input_device_index=self.mic_index)
        while not self.shutdown:
            try:
                time.sleep(.1)
            except Exception as error:
                pass
        self.stream.stop_stream()
        self.stream.close()
        self.p.terminate()

    def callback(self, in_data, frame_count, time_info, status):
        dispatcher.send(signal="Audio_In", data=in_data)
        return (None, pyaudio.paContinue)

    def record(self, data, frame_count, time_info, status_flags):
        if not self.shutdown:
            dispatcher.send(signal="Audio", sender=self, data=data)
            return ('NONE', 0)


def main():
    test = MicCapture()
    test.setDaemon(True)
    test.start()
    for i in range(10):
        time.sleep(1)
    test.shutdown = True
    test.join()


if __name__ == '__main__':
    main()
