import socket
import struct
import time
from threading import Thread
from pydispatch import dispatcher


class GetStream(Thread):
    """The Multicast Receiver thread"""

    def __init__(self, interface, multicast_group, multicast_port=50003):
        self.shutdown = False
        self.interface = interface
        self.multicast_group = multicast_group
        self.multicast_port = int(multicast_port)
        self.stream_num = None
        self.sample_rate = 48000
        Thread.__init__(self, name="Multicast Receiver")

    def run(self):
        server_address = (self.interface, self.multicast_port)
        # Create the socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # print "sock: ", sock.getsockname()
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # Bind to the server address
        sock.bind(server_address)
        # print "host: ", sock.getsockname()
        # Join

        group = socket.inet_aton(self.multicast_group)
        mreq = struct.pack('4sL', group, socket.INADDR_ANY)
        # print 'joining: ', repr(mreq)
        # print "group: ", self.multicast_group
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
        sock.setblocking(0)
        old_sequence = None
        self.dropped_total = 0
        self.received = 0
        while not self.shutdown:
            try:
                data, address = sock.recvfrom(1080)
                self.update_sample_rate(int(data[0]))
                # Take the values and bit shift then or
                self.stream_num = str(data[10] << 8 | data[6])
                # dispatcher.send(signal='Debug', sender=self, data=data[:14])
                sequence = data[9]
                # print(sequence)
                if old_sequence is None:
                    old_sequence = sequence
                # print '.',
                if (old_sequence + 1) != sequence:
                    drop = abs(sequence - old_sequence)
                    self.dropped_total += drop
                    # print '#',
                if sequence != 255:
                    old_sequence = sequence
                else:
                    old_sequence = -1
                # print 'Block: ', len(data)
                audio = data[14:]
                # print "RCVD Header: ", data[:14].encode('hex')
                # print('audio: ', repr(audio))
                # y = array.array('h', audio)
                # y.byteswap()
                # frame = binascii.hexlify(y).hex()
                # print('audio: ', type(audio))
                audio = bytearray(audio)
                audio[0::2], audio[1::2] = audio[1::2], audio[0::2]
                frame = bytes(audio)
                # bytes([c for t in zip(audio[1::2], audio[::2]) for c in t])
                # struct.pack('<2h', *struct.unpack('>2h', audio))

                self.received += 1
                dispatcher.send(signal="Audio", sender=dispatcher.Any, data=frame)
                # print(f"Sending seq: {sequence}")
            except BlockingIOError as error:
                # no stream detected
                # lets wait a bit and check again
                time.sleep(.1)
        # print 'dropping: ', repr(mreq)
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_DROP_MEMBERSHIP, mreq)
        sock.close()
        self.stream_num = ''

    def update_sample_rate(self, audio_info_byte):
        # print(f"Audio info byte: {audio_info_byte} {self.sample_rate}")
        if audio_info_byte == 0:
            self.sample_rate = 44100
        elif audio_info_byte == 1:
            self.sample_rate = 32000
        else:
            # audio info byte is 2 (or anything else)
            self.sample_rate = 48000

def main():
    test = GetStream(interface='172.17.0.26', multicast_group='239.255.0.4')
    test.setDaemon(True)
    test.start()
    time.sleep(20)
    test.shutdown = True
    test.join()


if __name__ == '__main__':
    main()
