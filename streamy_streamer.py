import wx
import wx.html
import os
import json
import logging
import streamy_gui
import ipaddress
import math
import sys
import webbrowser
import queue
import pyaudio
from scripts import (interface_monitor, streamy_api, streamy_mic_capture,
                     streamy_multicast_receiver, streamy_audio_player, streamy_audio_recorder,
                     streamy_multicast_transmitter)

from license_base import LicenseBase
from wx.lib.wordwrap import wordwrap

from pydispatch import dispatcher

logger = logging.getLogger("Streamy")
logger.setLevel(logging.INFO)


class Streamy_Frame(streamy_gui.StreamyFrame):
    def __init__(self, parent):
        streamy_gui.StreamyFrame.__init__(self, parent)

        icon_bundle = wx.IconBundle()
        icon_bundle.AddIcon(self.resource_path(os.path.join("icon", "st.ico")), wx.BITMAP_TYPE_ANY)
        self.SetIcons(icon_bundle)
        self.name = "Streamy"
        self.version = "v1.0.2"
        self.SetTitle(self.name + " " + self.version)
        self.receiver = None
        self.playing = False
        self.mic_recording_on = False
        self.recording = False
        self.record_path = None
        self.load_config()
        self.load_api()
        self.old_count = 0
        self.redraw_timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.on_refresh, self.redraw_timer)
        self.redraw_timer.Start(1000)

        self.interface_list = []

        self.interface_queue = queue.Queue()
        ip_monitor = interface_monitor.InterfaceMonitor(self.interface_queue)
        ip_monitor.setDaemon(True)
        ip_monitor.start()

        self.populate_mics()

        dispatcher.connect(self.interface_update, signal="Interface Update")

        self.interface_queue.put(['send_update'])

        self.license_thread = LicenseBase(name=self.name, version=self.version, message_callback=self.license_message)
        self.license_thread.daemon = True
        self.license_thread.start()

        self.Fit()

    def populate_mics(self):
        try:
            p = pyaudio.PyAudio()
            info = p.get_host_api_info_by_index(0)
            number_devices = info.get('deviceCount')
            self.mic_index_lookup = []
            for i in range(number_devices):
                device = p.get_device_info_by_host_api_device_index(0, i)
                channels = device.get('maxInputChannels')
                if channels > 0:
                    self.mic_index_lookup.append((i, channels, device.get('name')))
            if self.mic_index_lookup:
                self.mic_cmb.AppendItems([device[2] for device in self.mic_index_lookup])
                self.mic_cmb.SetSelection(0)
        except Exception as error:
            logger.critical(f"Unable to populate mics: {repr(error)}")

    def resource_path(self, relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            base_path = sys._MEIPASS
        except Exception:
            base_path = os.path.abspath(".")

        return os.path.join(base_path, relative_path)

    def interface_update(self, interface_list):
        self.ip_address_list = []
        self.tx_ip_choice_cmb.Clear()
        self.rx_ip_choice_cmb.Clear()
        for interface in interface_list:
            self.ip_address_list.append(interface.ip_addresses[0])
            self.tx_ip_choice_cmb.Append(interface.ip_addresses[0])
            self.rx_ip_choice_cmb.Append(interface.ip_addresses[0])
        self.tx_ip_choice_cmb.SetSelection(0)
        self.rx_ip_choice_cmb.SetSelection(0)
        if self.mic_recording_on:
            self.on_mic_toggle(None)
        if self.playing:
            self.on_play_toggle(None)

    def on_tx_ip(self, event):
        if self.mic_recording_on:
            self.on_mic_toggle(event)

    def on_rx_ip(self, event):

        self.on_rx_text_entry(None)

        if self.playing:
            self.on_play_toggle(event)

    def on_refresh(self, event):
        """Update the stream number"""
        self.interface_queue.put(['check_changes'])
        try:
            self.rx_id_txt.SetLabel(self.receiver.stream_num)
        except AttributeError:
            self.rx_id_txt.SetLabel('')
        except TypeError:
            self.rx_id_txt.SetLabel('Waiting for stream')
        except Exception as error:
            print("on refresh: ", repr(error))

        if self.receiver:
            if self.receiver.sample_rate == 32000 and self.sample_rate_text.GetStringSelection() != "32000":
                if self.playing:
                    self.play_stop()
                self.sample_rate_text.SetSelection(0)
            elif self.receiver.sample_rate == 44100 and self.sample_rate_text.GetStringSelection() != "44100":
                if self.playing:
                    self.play_stop()
                self.sample_rate_text.SetSelection(1)
            elif self.receiver.sample_rate == 48000 and self.sample_rate_text.GetStringSelection() != "48000":
                if self.playing:
                    self.play_stop()
                self.sample_rate_text.SetSelection(2)

        try:
            # print self.recorder.size
            self.record_size_txt.SetLabel(self.convert_size(self.recorder.size))
        except AttributeError:
            pass
        except Exception as error:
            print("on refresh: ", repr(error))
        try:
            self.rx_dropped_txt.SetLabel(str(self.receiver.dropped_total))
            self.rx_rcvd_txt.SetLabel(str(self.receiver.received))
            if self.old_count < self.receiver.received:
                # looks like we are getting a stream
                self.play_btn.SetBackgroundColour('green')
                self.play_btn.Refresh()
                self.old_count = self.receiver.received
            else:
                self.play_btn.SetBackgroundColour(wx.NullColour)
                self.play_btn.Refresh()
                self.old_count = self.receiver.received
        except AttributeError:
            pass
        except Exception as error:
            print("on refresh: ", repr(error))

        try:
            self.tx_sent_txt.SetLabel(str(self.mic_streamer.block_count))
        except AttributeError:
            pass
        except Exception as error:
            print("on refresh: ", repr(error))

    def on_tx_text_entry(self, event):
        """Checks if valid stream or group"""
        stream_number = self.tx_txt_input.GetValue()
        # if stream_number == self.rx_txt_input.GetValue():
        #     feedback = 'invalid'
        #     group = None
        # else:
        if self.tx_interleave_chk.GetValue():
            feedback, group, stream = self.check_stream_or_group_interleave(stream_number, 'tx')
        else:
            feedback, group, stream = self.check_stream_or_group(stream_number, 'tx')
        self.stream_feedback(feedback, 'tx')
        if feedback == "valid":
            self.tx_group_txt.SetLabel(group + ':50003')
            self.tx_id_txt.SetLabel(stream)
            self.mic_btn.Enable(True)
            self.save_config()
        else:
            self.tx_group_txt.SetLabel('')
            self.tx_id_txt.SetLabel('')
            self.mic_btn.Enable(False)

    def on_rx_text_entry(self, event):
        """Checks if valid stream or group"""
        self.stop_multicast_receiver()
        self.rx_dropped_txt.SetLabel('')
        self.rx_rcvd_txt.SetLabel('')
        stream_number = self.rx_txt_input.GetValue()
        # if stream_number == self.tx_txt_input.GetValue():
        #     feedback = 'invalid'
        #     group = None
        # else:
        if self.rx_interleave_chk.GetValue():
            feedback, group, stream = self.check_stream_or_group_interleave(stream_number, 'rx')
        else:
            feedback, group, stream = self.check_stream_or_group(stream_number, 'rx')
        self.stream_feedback(feedback, 'rx')

        if group is not None:
            # print "start stream: ", group
            self.start_multicast_receiver(group)
            self.save_config()

    def check_stream_or_group_interleave(self, text, rx_or_tx):
        """Checks if we have a valid stream or group in interleave mode """
        try:
            if "." in text:
                # We have a group address
                group = text
            elif text == "":
                return 'warn', None, None
            elif str(text).isdigit():
                # We have a stream number
                # First we check if it is "invalid"
                if (int(text)) % 128 == 0:
                    # print "modo"
                    return 'invalid', None, None
                group = self.convert_stream_to_multicast_interleave(text)
                # print 'group: ', group
            else:
                # print 'not a digit, not text?'
                return 'invalid', None, None
        except Exception as error:
            print("GetValue error: ", repr(error))
            return 'invalid', None, None

        try:
            if ipaddress.ip_address(group).is_multicast:
                last = group.split('.')[-1]
                if last in ['0', '255']:
                    # print 'last 0 or 255'
                    return 'invalid', None, None
                group_list = group.split('.')
                # print "group: ", group_list[2], group_list[3]
                if (group_list[0] != '239' or group_list[1] != '255' or int(group_list[3]) % 2 or (group_list[2] == "255" and group_list[3] == "254")):
                    # print 'doesnt start with 239.255 and the last octet is odd, and the last two are not "255.254"'
                    return 'invalid', None, None
                stream = str(self.convert_multicast_to_stream_interleave(group))
                return 'valid', group, stream
        except ValueError:
            # not an multicast address
            # print 'not multicast address'
            return 'invalid', None, None
        except Exception as error:
            print("Stream or Group Error: ", repr(error))
            return 'invalid', None, None

    def convert_stream_to_multicast_interleave(self, stream):
        """Converts a stream number to a interleave multicast group"""
        stream = int(stream)
        if stream // 128 == 0:
            a = 0
        else:
            a = stream // 128
        b = (stream % 128) * 2
        group = u"239.255.{}.{}".format(str(a), str(b))
        # print 'convert stream to multicast: ', group
        return group

    def convert_multicast_to_stream_interleave(self, group):
        """Converts a interleave multicast group to a stream number"""

        a, b = group.split('.')[2:]
        a = int(a) * 128
        b = int(b) / 2
        # if int(a) == 165:
        #     a = 0
        # else:
        #     a = int(a) - 128
        #     a = a * 256
        # print "a: ", a, "b: ", b
        return int(a) + int(b)

    def check_stream_or_group(self, text, rx_or_tx):
        """Checks if we have a valid stream or group """
        try:
            if "." in text:
                # We have a group address
                group = text
            elif text == "":
                return 'warn', None, None
            elif str(text).isdigit():
                # We have a stream number
                # First we check if it is "invalid"
                if (int(text) + 1) % 256 == 0 or (int(text)) % 256 == 0 or int(text) == 0:
                    # print "modo"
                    return 'invalid', None, None
                group = self.convert_stream_to_multicast(text)
            else:
                # print 'not a digit, not text?'
                return 'invalid', None, None
        except Exception as error:
            print("GetValue error: ", repr(error))
            return 'invalid', None, None

        try:
            if ipaddress.ip_address(group).is_multicast:
                last = group.split('.')[-1]
                if last in ['0', '255']:
                    # print 'last 0 or 255'
                    return 'invalid', None, None
                group_list = group.split('.')
                if (group_list[0] != '239' or group_list[1] != '255' or int(group_list[2]) < 128):
                    # print "doesn't start with 239.255 or 3 octet less than 128"
                    return 'invalid', None, None
                stream = str(self.convert_multicast_to_stream(group))
                return 'valid', group, stream
        except ValueError:
            # not an multicast address
            # print 'not multicast address'
            return 'invalid', None, None
        except Exception as error:
            print("Stream or Group Error: ", repr(error))
            return 'invalid', None, None

    def convert_stream_to_multicast(self, stream):
        """Converts a stream number to a multicast group"""
        stream = int(stream)
        if stream // 256 == 0:
            a = 165
        elif stream / 256 == 37:
            a = 128
        else:
            a = stream // 256 + 128
        b = stream % 256
        group = u"239.255.{}.{}".format(str(a), str(b))
        # print 'convert stream to multicast: ', group
        return group

    def convert_multicast_to_stream(self, group):
        """Converts a multicast group to a stream number"""

        a, b = group.split('.')[2:]
        if int(a) == 165:
            a = 0
        elif int(a) == 128:
            a = 37 * 256
        else:
            a = int(a) - 128
            a = a * 256
        # print "a: ", a, "b: ", b
        return int(a) + int(b)

    def stream_feedback(self, notification, item):
        """Feed back on invalid stream"""
        if item == 'rx':
            if notification == "warn":
                self.rx_txt_input.SetBackgroundColour('yellow')
                self.rx_txt_input.Refresh()
            if notification == "invalid":
                self.rx_txt_input.SetBackgroundColour((232, 81, 81))
                self.rx_txt_input.Refresh()
            if notification == "valid":
                self.rx_txt_input.SetBackgroundColour(wx.NullColour)
                self.rx_txt_input.Refresh()

        if item == 'tx':
            if notification == "warn":
                self.tx_txt_input.SetBackgroundColour('yellow')
                self.tx_txt_input.Refresh()
            if notification == "invalid":
                self.tx_txt_input.SetBackgroundColour((232, 81, 81))
                self.tx_txt_input.Refresh()
            if notification == "valid":
                self.tx_txt_input.SetBackgroundColour(wx.NullColour)
                self.tx_txt_input.Refresh()

    def start_multicast_receiver(self, group):
        self.receiver = streamy_multicast_receiver.GetStream(interface=self.rx_ip_choice_cmb.GetStringSelection(), multicast_group=group)
        self.receiver.setDaemon(True)
        self.receiver.start()
        self.rx_group_txt.SetLabel(group + ':50003')
        # self.m_port_txt.SetLabel('50003')
        self.play_btn.Enable(True)
        self.record_btn.Enable(True)

    def stop_multicast_receiver(self):
        try:
            if hasattr(self.receiver, 'shutdown'):
                self.receiver.shutdown = True
                self.receiver.join()
                # del self.listener
                self.rx_group_txt.SetLabel('')
                # self.m_port_txt.SetLabel('')
                self.play_btn.Enable(False)
                self.record_btn.Enable(False)
                self.play_stop()
        except AttributeError:
            pass
        except Exception as error:
            print("Stop stream receiver error: ", repr(error))

    def on_mic_toggle(self, event):
        """Turn the mic on or off"""
        # print 'Toggle'
        if self.mic_recording_on:
            self.mic_off()
        else:
            self.mic_on()

    def mic_on(self):
        group, port_number = self.tx_group_txt.GetValue().split(':')
        stream_number = self.tx_id_txt.GetValue()
        self.tx_txt_input.Disable()
        self.tx_ip_choice_cmb.Disable()
        self.mic_cmb.Disable()
        self.tx_interleave_chk.Disable()
        mic = self.mic_index_lookup[self.mic_cmb.GetCurrentSelection()]
        self.mic_recorder = streamy_mic_capture.MicCapture(num_channels=mic[1], mic_index=mic[0])
        self.mic_recorder.setDaemon(True)
        self.mic_recorder.start()
        self.mic_streamer = streamy_multicast_transmitter.MulticastTransmitter(interface=self.tx_ip_choice_cmb.GetStringSelection(),
                                                                               multicast_group=group,
                                                                               stream_number=stream_number)
        self.mic_streamer.setDaemon(True)
        self.mic_streamer.start()
        self.mic_btn.SetLabel('Stop')
        self.mic_recording_on = True

    def mic_off(self):
        if self.mic_recording_on:
            self.mic_recorder.shutdown = True
            self.mic_recorder.join()
            self.mic_streamer.shutdown = True
            self.mic_streamer.join()
            self.mic_btn.SetLabel('Send Mic')
            self.mic_recording_on = False
            self.tx_txt_input.Enable()
            self.mic_cmb.Enable()
            self.tx_ip_choice_cmb.Enable()
            self.tx_interleave_chk.Enable()

    def on_record_toggle(self, event):
        """Either start or stop recording"""
        if self.recording:
            self.record_stop()
        else:
            self.record_start()

    def record_start(self):
        """Get the file name and start recording"""
        save_file_dialog = wx.FileDialog(self,
                                         message='Select file to save',
                                         # defaultDir=self.path,
                                         defaultFile="",
                                         wildcard="WAV files (*.wav)|*.wav",
                                         style=wx.FD_SAVE)
        if save_file_dialog.ShowModal() == wx.ID_OK:
            self.record_path = save_file_dialog.GetPath()
            self.recorder = streamy_audio_recorder.AudioRecorder(self.record_path)
            self.recorder.setDaemon(True)
            self.recorder.start()
            self.recording = True
        else:
            self.record_btn.SetValue(False)

    def record_stop(self):
        """Stop recording"""
        if self.recording:
            self.recorder.shutdown = True
            self.recorder.join()
            self.recording = False

    def on_play_toggle(self, event):
        """Either play or stop """
        if self.playing:
            self.play_stop()
        else:
            self.play_start()

    def play_start(self):
        """Play the stream"""
        self.player = streamy_audio_player.AudioPlayer(sample_rate=self.receiver.sample_rate)
        self.player.setDaemon(True)
        self.player.start()
        self.rx_txt_input.Disable()
        self.rx_interleave_chk.Disable()
        self.rx_ip_choice_cmb.Disable()
        self.play_btn.SetLabel('Stop')
        self.playing = True

    def play_stop(self):
        """Stop the stream"""
        if self.playing:
            dispatcher.send("PlayShutdown")
            self.player.join()
            self.play_btn.SetLabel('Play')
            self.playing = False
            self.rx_txt_input.Enable()
            self.rx_ip_choice_cmb.Enable()
            self.rx_interleave_chk.Enable()

    def convert_size(self, size_bytes):
        if size_bytes == 0:
            return "0B"
        size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
        i = int(math.floor(math.log(size_bytes, 1024)))
        p = math.pow(1024, i)
        s = round(size_bytes // p, 2)
        return "%s %s" % (s, size_name[i])

    def on_close(self, event):
        """Close all threads"""
        try:
            self.receiver.shutdown = True
            self.receiver.join()
        except AttributeError:
            pass
        try:
            self.player.shutdown = True
            self.player.join()
        except AttributeError:
            pass
        try:
            self.recorder.shutdown = True
            self.recorder.join()
        except AttributeError:
            pass
        try:
            self.mic_recorder.shutdown = True
            self.mic_recorder.join()
        except AttributeError:
            pass
        try:
            self.mic_streamer.shutdown = True
            self.mic_streamer.join()
        except AttributeError:
            pass
        try:
            self.stream_api.shutdown = True
            self.stream_api.join()
        except AttributeError:
            pass
        self.Destroy()

    def load_api(self):
        """Starts the API interface"""
        self.api = streamy_api.StreamAPI(self)
        self.api.setDaemon(True)
        self.api.start()

    def load_config(self):
        """Loads the saved config"""
        try:
            with open('streamy.conf', 'r') as f:
                config = json.load(f)
            if "rx_stream" in config:
                self.rx_txt_input.SetValue(config['rx_stream'])
            if "rx_interleave" in config:
                self.rx_interleave_chk.SetValue(config['rx_interleave'])
            if "tx_stream" in config:
                self.tx_txt_input.SetValue(config['tx_stream'])
            if "tx_interleave" in config:
                self.tx_interleave_chk.SetValue(config['tx_interleave'])

        except Exception as error:
            print("unable to load configuration: ", error)

    def save_config(self):
        """Save the ip addresses for next time"""
        try:
            with open('streamy.conf', 'w') as f:
                config = {"rx_stream": self.rx_txt_input.GetValue(),
                          "rx_interleave": self.rx_interleave_chk.GetValue(),
                          "tx_stream": self.tx_txt_input.GetValue(),
                          "tx_interleave": self.tx_interleave_chk.GetValue(),
                          }
                json.dump(config, f)
        except Exception as error:
            print("unable to save configuration: ", error)

    def on_documentation(self, event):
        aboutDlg = streamy_gui.AboutFrame(None)
        html = wx.html.HtmlWindow(aboutDlg)
        with open(self.resource_path(os.path.join('doc', 'api_doc.html')), 'rb') as f:
            html.SetPage(f.read())
        aboutDlg.Show()

    def update_required(self, url):
        """Show the update page"""
        dlg = wx.MessageDialog(parent=self,
                               message='An update is available. \rWould you like to go to the download page?',
                               caption='Update available',
                               style=wx.OK | wx.CANCEL)

        if dlg.ShowModal() == wx.ID_OK:
            webbrowser.open(url)

    def license_message(self, data):
        """We either request a serial number or show a message"""
        if "get_serial" in data:
            wx.CallAfter(self.license_get_serial, data['message'])
        if "show_message" in data:
            wx.CallAfter(self.license_show_message, data['message'])
        if "manual_activation" in data:
            wx.CallAfter(self.save_manual_activation, data['cipher_text'], data['serial'], data['stage_two'])
        if "complete" in data:
            self.Show()
            if "update_url" in data:
                update_url = data['update_url']
                if update_url:
                    self.update_required(url=update_url)

    def save_manual_activation(self, cipher_text, serial, stage_two):
        self.Hide()
        if stage_two:
            # Stage two
            open_file_dialog = wx.FileDialog(self, message="Load Manual Activation",
                                             defaultDir=self.storage_path,
                                             defaultFile="",
                                             wildcard="Custom Files (*.lic;)|*.lic;",
                                             style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
            if open_file_dialog.ShowModal() == wx.ID_OK:
                license_path = open_file_dialog.GetPath()
                success = self.license_thread.manual_activation(license_path, serial)
                if success:
                    self.Show()
                    return
                else:
                    dlg = wx.MessageDialog(parent=self,
                                           message=wordwrap("Unable to use that file for manual activation", 350, wx.ClientDC(self)),
                                           caption='Unlicensed',
                                           style=wx.OK)
                    dlg.ShowModal()

        dlg = wx.MessageDialog(parent=self,
                               message=wordwrap("Unable to contact server.\nWould you like to download the manual activation?", 350, wx.ClientDC(self)),
                               caption='Unlicensed',
                               style=wx.OK | wx.CANCEL)
        if dlg.ShowModal() == wx.ID_OK:
            dlg = wx.FileDialog(self,
                                message='Save manual activation',
                                defaultDir=self.storage_path,
                                defaultFile=f"{serial}.txt",
                                wildcard="TXT files (*.txt)|*.txt",
                                style=wx.FD_SAVE)
            if dlg.ShowModal() == wx.ID_OK:
                file_path = dlg.GetPath()
                with open(file_path, 'w') as cipher_text_file:
                    cipher_text_file.write(cipher_text)
        wx.CallAfter(self.on_exit, None)

    def license_get_serial(self, message):
        self.Hide()
        dlg = wx.TextEntryDialog(parent=self,
                                 message=f"{message}\nPlease enter your serial number",
                                 caption=f'{self.name} Serial Number Required',
                                 style=wx.OK | wx.CANCEL)

        if dlg.ShowModal() == wx.ID_OK:
            serial = dlg.GetValue()
            self.license_thread.verify_serial_with_server(serial)
            self.Show()
        else:
            self.license_show_message("This software requires a valid serial number to run.")

    def license_show_message(self, message: str, exit: bool = True):
        dlg = wx.MessageDialog(parent=self,
                               message=wordwrap(message, 350, wx.ClientDC(self)),
                               caption='Unlicensed',
                               style=wx.OK)
        dlg.ShowModal()
        if exit:
            wx.CallAfter(self.on_exit, None)


def main():
    """run the main program"""
    st_app = wx.App()  # redirect=True, filename="log.txt")
    # do processing/initialization here and create main window
    st_frame = Streamy_Frame(None)
    st_frame.Show()
    st_app.MainLoop()


if __name__ == '__main__':
    main()
