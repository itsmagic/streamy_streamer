import socket
import struct
import time

server_address = ('', 50003)
# Create the socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# Bind to the server address

# print "host: ", sock.getsockname()
# Join
group = socket.inet_aton('239.255.165.12')
mreq = struct.pack('4sL', group, socket.INADDR_ANY)
# print 'joining: ', repr(mreq)
print socket.IPPROTO_IP
print socket.SOL_IP
sock.setsockopt(socket.SOL_IP, socket.IP_ADD_MEMBERSHIP, mreq)
# sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

sock.bind(server_address)
sock.setblocking(0)
time.sleep(10)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_DROP_MEMBERSHIP, mreq)
sock.close()
